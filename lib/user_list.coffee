_ = require 'underscore'

module.exports =
  users: {},

  addUser: (user) ->
    @users[user['id']] = user

  removeUser: (user) ->
    delete @users[user['id']]

  userArray: ->
    _(@users).map (user) ->
      user
