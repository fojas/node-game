module.exports =
  newQuestion: ->
    @base = @generateNumber()
    @exponent = @generateNumber()
    @answer = Math.pow @base, @exponent
    @currentQuestion()

  generateNumber: ->
    Math.ceil(Math.random()*9)

  checkAnswer: (answer) ->
    @answer == parseInt(answer,10)

  currentQuestion: ->
    if !@base && !@exponent
      @newQuestion()
    base: @base, exponent:@exponent
