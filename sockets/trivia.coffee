_ = require 'underscore'

module.exports =
  # list of users playing the game
  userList: require '../lib/user_list'

  # source of questions and checker of answers
  triviaMaster: require '../lib/trivia_master'

  connection: (@socket) ->
    @socket.on "answer", _.bind(@answerQuestion, @)
    @socket.on "join", _.bind(@addUser, @)

  answerQuestion: (data) ->
    if @triviaMaster.checkAnswer data.response
      @givePoint data.userId
      @emitNewQuestion()

  givePoint: (userId) ->
    @userList.users[userId].score++
    @socket.broadcast.emit "addPoint", userId
    @socket.emit "addPoint", userId

  emitNewQuestion: ->
    newQuestion = @triviaMaster.newQuestion()
    @socket.broadcast.emit 'newQuestion', newQuestion
    @socket.emit 'newQuestion', newQuestion

  addUser: (user,fn) ->
    @socket.broadcast.emit "addUser", user
    @userList.addUser user
    @socket.emit 'newQuestion', @triviaMaster.currentQuestion()
    fn(@userList.userArray())
