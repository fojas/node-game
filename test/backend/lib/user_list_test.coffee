should = require 'should'
sinon = require 'sinon'
userList = require '../../../lib/user_list'

describe "UserList", ->
  it "can add users", ->
    user = {id:1, name:'Lucille'}
    userList.addUser user
    userList.users[1].should.equal(user)

  context "with a user list", ->
    beforeEach ->
      userList.addUser({id:id, name:'Gob', score:0}) for id in [1..5]

    it "can remove a user", ->
      userList.removeUser userList.users[4]
      should.not.exist userList.users[4]

    it "has the right number of users in the array", ->
      userList.userArray().length.should.equal 5

    it "has the correct format for users in the array", ->
      u = userList.userArray()[0]
      expected = userList.users[1]

      u.name.should.equal expected.name
      u.id.should.equal expected.id
      u.score.should.equal expected.score
