should = require 'should'
sinon = require 'sinon'
triviaMaster = require '../../../lib/trivia_master'

describe "TriviaMaster", ->
  context "when generating a question", ->
    question = null
    beforeEach ->
      question = triviaMaster.newQuestion()

    it "has a base", ->
      question.base.should.be.a 'number'
    it "has an exponent", ->
      question.exponent.should.be.a 'number'
    it "has the same base as the TriviaMaster", ->
      question.base.should.eql triviaMaster.base
    it "has the same exponent as the TriviaMaster", ->
      question.exponent.should.eql triviaMaster.exponent

  context "with a question", ->
    question = null
    beforeEach ->
      question = triviaMaster.newQuestion()

    it "has the answer", ->
      triviaMaster.answer.should.eql Math.pow(triviaMaster.base, triviaMaster.exponent)

    it "can check a correct answer", ->
      triviaMaster.checkAnswer(triviaMaster.answer).should.be.true
    it "can check a correct answer that is a string", ->
      triviaMaster.checkAnswer(''+triviaMaster.answer).should.be.true
    it "can check a incorrect answer", ->
      triviaMaster.checkAnswer(2*triviaMaster.answer).should.be.false
