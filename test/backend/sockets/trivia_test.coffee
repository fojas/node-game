should = require 'should'
sinon = require 'sinon'
triviaSocket = require '../../../sockets/trivia'
mockSocket = ->
  @on = sinon.spy()
  @emit = sinon.spy()
  @broadcast =
    emit: sinon.spy()
  return

describe "TriviaSocket", ->
  beforeEach =>
    @mockedSocket = new mockSocket
    triviaSocket.connection @mockedSocket

  it "listens for answers", =>
    @mockedSocket.on.calledWith("answer").should.be.true

  it "listens for joins", =>
    @mockedSocket.on.calledWith("join").should.be.true

  context "adding a user", =>
    beforeEach =>
      @user = {id:1, name:'Tobias', score:0}
      @spy = sinon.spy()
      triviaSocket.addUser @user, @spy

    it "calls back with user list", =>
      @spy.calledWith(triviaSocket.userList.userArray()).should.be.true
    it "broadcasts the user", =>
      @mockedSocket.broadcast.emit.calledWith("addUser", @user).should.be.true
    it "adds user to the list", =>
      triviaSocket.userList.users[@user.id].should.equal @user
    it "emits the question to the user", =>
      @mockedSocket.emit.calledWith 'newQuestion', triviaSocket.triviaMaster.currentQuestion()

  context "giving a user a point", =>
    beforeEach =>
      @user = {id:1, name:'Tobias', score:0}
      triviaSocket.userList.addUser @user
      triviaSocket.givePoint @user.id

    it "broadcast the added point", =>
      @mockedSocket.broadcast.emit.calledWith('addPoint', @user.id).should.be.true
    it "emits the added point", =>
      @mockedSocket.broadcast.emit.calledWith('addPoint', @user.id).should.be.true
    it "adds a point to the user", =>
      @user.score.should.equal 1

  context "answering a question", =>
    context "correctly", =>
      beforeEach =>
        @user = {id:1, name:'Tobias', score:0}
        sinon.spy(triviaSocket,'givePoint')
        sinon.spy(triviaSocket,'emitNewQuestion')
        triviaSocket.answerQuestion userId: @user.id, response:triviaSocket.triviaMaster.answer

      afterEach =>
        triviaSocket.givePoint.restore()
        triviaSocket.emitNewQuestion.restore()

      it "gives a point", =>
        triviaSocket.givePoint.calledWith(@user.id).should.be.true
      it "gives a new question", =>
        triviaSocket.emitNewQuestion.called.should.be.true

    context "incorrectly", =>
      beforeEach =>
        @user = {id:1, name:'Tobias', score:0}
        sinon.spy(triviaSocket,'givePoint')
        sinon.spy(triviaSocket,'emitNewQuestion')
        triviaSocket.answerQuestion userId: @user.id, response:triviaSocket.triviaMaster.answer+1

      afterEach =>
        triviaSocket.givePoint.restore()
        triviaSocket.emitNewQuestion.restore()

      it "gives a point", =>
        triviaSocket.givePoint.called.should.be.false
      it "gives a new question", =>
        triviaSocket.emitNewQuestion.called.should.be.false
