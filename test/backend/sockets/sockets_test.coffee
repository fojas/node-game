should = require 'should'
sockets = require '../../../sockets'

describe "sockets", ->
  it "has a trivia channel", ->
    should.exist sockets.trivia
  it "has a connection callback for trivia", ->
    should.exist sockets.trivia.connection
