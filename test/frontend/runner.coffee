express = require 'express'
handlebars = require 'hbs'
http = require 'http'
fs = require 'fs'
_ = require 'underscore'

handlebar_helpers = require('../../lib/handlebar_helpers')(handlebars)
app = module.exports = express()
server =  http.createServer(app)

app.configure () ->
  app.set 'views', "#{__dirname}"
  app.set 'view engine', 'hbs'
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use app.router
  app.use express.static("#{__dirname}")
  app.use express.static("#{__dirname}/../../public")
  app.use require('connect-assets') src : "#{__dirname}/../../public"

app.get '/', (req, res) ->
  assets = _(['models','views','sockets']).reduce(  (hash, attr) ->
    hash[attr] = fs.readdirSync "#{__dirname}/js/#{attr}"
    hash
  , {})
  res.render 'index',assets: assets

server.listen process.env.PORT || 4000
