var expect = chai.expect
describe("Question", function(){
  it( "should have an integer base", function(){
    var expected = 4;
    expect(new Model.Question({base:""+expected,exponent:1}).get('base')).to.eql(expected)
  });
  it( "should have an integer exponent", function(){
    var expected = 4;
    expect(new Model.Question({base:1,exponent:""+expected}).get('exponent')).to.eql(expected)
  });
  _(['base','exponent']).each(function(attr){
    var expected = 5, setterVal = ""+expected
    it("sets " + attr + " as an integer", function(){
      var model = new Model.Question;
      model.set(attr,setterVal);
      expect(model.get(attr)).to.eql(expected);
    });
  });
  it("triggers when answering", function(){
    var model = new Model.Question;
    try {
      sinon.spy(model,'trigger')
    } catch(e){
      model.answer(1);
      model.trigger.calledWith('answer',1)
    } finally{
      model.trigger.restore()
    }
  });
});
