describe("Model.User", function(){
  var user
    , userName = "Bob Loblaw";
  beforeEach(function(){
    user = new Model.User({name:userName})
  });

  it("has a name", function(){
    expect(user.get('name')).to.eql(userName);
  });

  it("defaults to a 0 score", function(){
    expect(user.get('score')).to.eql(0);
  });

  it("has a uniqueId", function(){
    var user = new Model.User;
    expect(user.id).to.exist
  });

  it("doesnt overwrite an existing id", function(){
    var expectedId= 123;
    var user = new Model.User({id:expectedId});
    expect(user.id).to.eql(expectedId);
  });

  it("adds a point to a user", function(){
    for(var i=1;i<5;i++){
      user.addPoint();
      expect(user.get('score')).to.eql(i)
    }
  });
});

describe("Collection.User", function(){
  var users;
  beforeEach(function(){
    users = new Collection.User;
    for(var i=10;i--;){
      users.add(new Model.User({score:Math.ceil(Math.random()*100)}));
    }
  });
  it("has the collection in order by score", function(){
    var currentScore = users.first().get('score');;
    users.each(function(u){
      expect(u.get('score')).to.be.at.most(currentScore);
      currentScore = u.get('score');
    });
  })
});
