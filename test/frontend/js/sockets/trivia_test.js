describe("Socket.Trivia", function(){
  var socket, user, spy;
  beforeEach(function(){
    user = new Model.User({name:'Bob Loblaw'});
    socket = new Socket.Trivia;
    spy = sinon.spy(socket.socket,"emit");
  });

  afterEach(function(){
    socket.socket.emit.restore();
  });

  it("has a Socket.io connection", function(){
    expect(socket.socket.socket).to.exist;
  });
  it("is connected to the trivia socket", function(){
    expect(socket.socket.name).to.eql("/trivia");
  });

  context("joining a game", function(){
    beforeEach(function(){
      socket.join(user)
    });
    it("emits an join", function(){
      expect(spy.getCall(0).args[0]).to.eql('join')
    });
    it("join with a userId", function(){
      expect(spy.getCall(0).args[1]['id']).to.eql(user.id)
    });
    it("joins with the name", function(){
      expect(spy.getCall(0).args[1]['name']).to.eql(user.get('name'))
    });
  });

  context("answering a question", function(){
    var answer = 100;
    beforeEach(function(){
      socket.answer(user,answer);
    });
    it("emits an answer", function(){
      expect(spy.getCall(0).args[0]).to.eql('answer')
    });
    it("answers with a userId", function(){
      expect(spy.getCall(0).args[1]['userId']).to.eql(user.id)
    });
    it("answers with the answer", function(){
      expect(spy.getCall(0).args[1]['response']).to.eql(answer)
    });
  });

  context("listening to broadcasts", function(){
    var mockedSocket
      , mockSocket = function(){ this.on = sinon.spy(); }

    beforeEach(function(){
      sinon.stub(io,'connect').returns(mockedSocket = new mockSocket)
    });

    it("listening for channels listens on the io socket", function(){
      s = new Socket.Trivia;
      s.on('foo');
      expect(mockedSocket.on.calledWith('foo')).to.be.true;
    });
  });
});
