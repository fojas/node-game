describe("View.Scoreboard", function(){
  beforeEach(function(){
    fixtures.load('../fixtures/scoreboard.html')
  });

  it("has a scoreboard", function(){
    expect((new View.Scoreboard({collection: new Collection.User})).$el.length).to.eql(1)
  });

  context("with a scoreboard", function(){
    var scoreboard, users;
    beforeEach(function(){
      users = new Collection.User;
      scoreboard = new View.Scoreboard({collection:users});
    });
    it("adds users to a scoreboard", function(){
      for(var i = 1; i<=5;i++){
        users.add(new Model.User({name:'Gob',score:5}));
        expect(scoreboard.$('li').length).to.eql(i);
      }
    });
    context("with some users", function(){
      beforeEach(function(){
        for(var i = 1; i<=5;i++){
          users.add(new Model.User({name:'Gob',score:5}));
        }
      });
      it("can find a user view", function(){
        users.each(function(user){
          var view = scoreboard.getUserViewById(user.id);
          expect(view.$el.attr('id')).to.eql('scoreboard_user_'+user.id)
        });
      });
    });
  });

  afterEach(function(){
    fixtures.cleanUp();
  });

});

describe("View.Scoreboard.User", function(){
  beforeEach(function(){
    fixtures.load('../fixtures/scoreboard.html')
  });

  context("when rendering a user", function(){
    var userView, user;
    beforeEach(function(){
      user =  new Model.User({name:'Gob',score:5});
      userView = new View.Scoreboard.User({model:user}).render();
    });

    it("has the name", function(){
      expect(userView.$('.name').html()).to.eql(user.get('name'));
    });
    it("has the score", function(){
      expect(userView.$('.score').html()).to.eql(""+user.get('score'));
    });
    it("has the id", function(){
      expect(userView.$el[0].id).to.eql('scoreboard_user_'+user.get('id'))
    });

    it("updates the score", function(){
      user.set('score', 8);
      expect(userView.$('.score').html()).to.eql(""+user.get('score'));
    });
  });
});
