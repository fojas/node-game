describe("View.Question", function(){
  beforeEach(function(){
    fixtures.load('../fixtures/question.html')
  });

  describe("changing the question", function(){
    var model, view
    beforeEach(function(){
      model = new Model.Question;
      view  = new View.Question({model:model});
    });

    it("changes the exponent html", function(){
      for(var exponent = 10;exponent--;){
        model.set({base:11,exponent:exponent});
        expect(view.exponent.html()).to.eql(exponent.toString())
      }
    });

    it("changes the base html", function(){
      for(var base = 10;base--;){
        model.set({base:base,exponent:11});
        expect(view.base.html()).to.eql(base.toString())
      }
    });
  });

  context("submitting a response", function(){
    var model, view
    beforeEach(function(){
      model = new Model.Question;
      view  = new View.Question({model:model});
      sinon.spy(model,'answer')
      view.answer.val('99');
      view.form.submit();
    });

    it("clears the form", function(){
      expect(view.answer.val()).to.eql("");
    });

    it("answers through the model", function(){
      expect(model.answer.calledWith("99")).to.be.true
    });
  });

  afterEach(function(){
    fixtures.cleanUp();
  });
});
