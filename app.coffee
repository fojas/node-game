io = require 'socket.io'
express = require 'express'
handlebars = require 'hbs'
http = require 'http'
_ = require 'underscore'

routes = require './routes'
sockets = require './sockets'
handlebar_helpers = require('./lib/handlebar_helpers')(handlebars)
app = module.exports = express()
server =  http.createServer(app)

app.configure () ->
  app.set 'views', "#{__dirname}/views"
  app.set 'view engine', 'hbs'
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use app.router
  app.use express.static("#{__dirname}/public")
  app.use require('connect-assets') src : "#{__dirname}/public"

app.configure 'development', () ->
  app.use express.errorHandler( dumpExceptions: true, showStack: true )

app.configure 'production', () ->
  app.use express.errorHandler()

app.get '/', routes.index

server.listen process.env.PORT || 3000
sio = io.listen(server)
  
# create a Socket.io endpoint for any socket object in the sockets folder
# this will allow creating of multiple endpoints if needed.
for own attr, command of sockets
  sio.of('/'+attr).on 'connection', _.bind(command.connection, command)

