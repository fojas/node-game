View.Scoreboard = Backbone.View.extend
  initialize: ->
    @$el = $('#scoreboard')
    @collection.on('add', _.bind(@addOne,this))
    @userViews = {}

  # add a user subview for each user in the collection
  addOne: (model) ->
    userView = (new View.Scoreboard.User {model:model}).render()
    @userViews[model.id] = userView
    @$el.append(userView.$el)

  getUserViewById: (id) ->
    @userViews[id]

# user subview
View.Scoreboard.User = Backbone.View.extend {
  initialize: ->
    @template = @constructor.template()
    @model.on 'change:score', _.bind(@updateScore, @)

  updateScore: ()->
    @$('.score').html @model.get('score')

  render: ->
    @$el = $ @template(@model.toJSON()).trim()
    this

}, {
  template: ->
    @_template = @_template || Handlebars.compile $('#scoreboard_user-template').html()
}
