View.Question = Backbone.View.extend
  initialize: ->
    @$el = $('#question')
    @base = @$ '#base'
    @exponent = @$ "#exponent"
    @form = @$ 'form'
    @answer = @$ '#answer'
    @model.on 'change', _.bind(@changeQuestion,@)

    # submit the form throgh the socket
    @form.on 'submit', _.bind(@submitAnswer, @)
    this

  changeQuestion: ->
    @base.html @model.get('base')
    @exponent.html @model.get('exponent')

  submitAnswer: (e) ->
    e.preventDefault()
    @model.answer(@answer.val())
    @answer.val('')
