# simple wrapper around socket.io to act as a service between the backend and frontend

class Socket.Trivia
  constructor: () ->
    @socket = io.connect('/trivia')
    @

  on: (channel, fn) ->
    @socket.on channel, fn

  join: (user,fn) ->
    @socket.emit 'join', user.toJSON(), fn

  answer: (user, response) ->
    @socket.emit 'answer', {userId: user.id, response:response}
