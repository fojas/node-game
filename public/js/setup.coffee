# setup the namespaces
window.Socket = {}
window.Model= {}
window.Collection= {}
window.View= {}

$ ->
  if $('#question').length
    names = ['Gob','Tobias','Buster','Lucille','Oscar','Bob Loblaw','Annyong','Steve Holt','Franklin']

    # this guy transports data back and forth
    triviaSocket = new Socket.Trivia

    # current user model, name chosen at random for now
    user = new Model.User name:"#{names[Math.floor(Math.random()*names.length)]}"

    # representation of the current question
    question = new Model.Question

    # these are all the other users online
    users = new Collection.User

    # View for all the other users and their scores
    scoreboardView = new View.Scoreboard collection:users

    # View for the question
    questionView = new View.Question model: question

    # when a user joins, add them to the list of users
    triviaSocket.on 'addUser', _.bind(users.add, users)

    # if a user earns a point, add it to the scorebard
    triviaSocket.on 'addPoint', _.bind(users.addPoint, users)

    # change the question if a new one comes in
    triviaSocket.on 'newQuestion', _.bind(question.set, question)

    # add the current user to everyones scorebard
    triviaSocket.join user, _.bind(users.add, users)

    # send a response to the backend when answering a question
    question.on 'answer', (answer) ->
      triviaSocket.answer.call triviaSocket, user, answer
