Model.User = Backbone.Model.extend
  initialize: ->
    @ensureDefaults()

  ensureDefaults: ->
    # give the user object an ID if it doesnt have one
    if !@id
      @set('id', @id = (new Date)*1 + ~~(Math.random()*100))

    # give the user a score if it doesnt have one
    if !@get('score')
      @set('score', 0)

  addPoint: ->
    @set 'score', @get('score') + 1

Collection.User = Backbone.Collection.extend
  model: Model.User

  # method for sorting users by score DESC
  # http://stackoverflow.com/a/13063998/899841
  comparator: (user) ->
    -1 * user.get 'score'

  addPoint: (userId) ->
    @get(userId).addPoint();
