Model.Question = Backbone.Model.extend

  # override set method so that base and exponent are
  # integers
  set: (key, value, options) ->
    if _.isObject(key) || key == null
      attrs = key
      options = value
    else
      attrs = {}
      attrs[key] = value

    for own attr,val of attrs
      if _(['base','exponent']).contains attr
        attrs[attr] = parseInt val, 10

    Backbone.Model.prototype.set.call this, attrs, options

  # application is bound to the answer event to send
  # responses to the socket connection 
  answer: (response)->
    @trigger 'answer', response
