# Power Ranger Trivia

A game about knowing your exponents

# Setup Instructions

## node modules

Node modules are easily setup:

    $ npm install

## Starting the app

Start the app then go to http://localhost:3000

    $ npm start


# Testing

## frontend

Start the mocha server then go to http://localhost:4000

    $ node test.js

## backend

There is an npm script command for running the tests.

    $ npm test
